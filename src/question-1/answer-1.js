function reverse(array){
    let final_array = [];
    const array_length = array.length;

    for (let i = array_length - 1; i > -1; i--){
        final_array.push(array[i]);
    }

    return final_array;
}