import React from "react"
import { screen, waitFor } from "@testing-library/react"
import { render } from "./test-utils"
import { App } from "./App"

test("renders cat facts", async () => {
  render(<App />)
  
  await waitFor(() => {
    expect(screen.queryByTestId('fact-grid')).toBeInTheDocument()
  })
})
