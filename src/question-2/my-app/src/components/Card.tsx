import { Box, Flex, FlexProps, useColorModeValue } from '@chakra-ui/react'
import * as React from 'react'


export const Card = (props: FlexProps) => {
  const { children, ...rest } = props
  return (
    <Flex
      direction="column"
      alignItems="center"
      rounded="md"
      padding="8"
      position="relative"
      bg={useColorModeValue('white', 'gray.700')}
      shadow={{ md: 'base' }}
      {...rest}
    >
      {children}
    </Flex>
  )
}