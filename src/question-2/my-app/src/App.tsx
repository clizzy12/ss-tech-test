import * as React from "react"
import {
  ChakraProvider,
  Box,
  SimpleGrid,
  Text,
  useColorModeValue,
  theme,
} from "@chakra-ui/react"
import { Card } from './components/Card'
import axios from "axios";

interface CatFactsInterface {
  fact: string,
  length: number
}


export const App = () => {
  const [ facts, setFacts ] = React.useState<Array<CatFactsInterface>>([]);
  const [ error, setErrorMessage ] = React.useState<string>('');

  async function loadData() {
    const response = await axios.get(`https://catfact.ninja/facts?limit=9`);
    console.log(response.data.data);    
    let sortedFacts = [];

    if (response && response.data && response.data.data && response.data.data.length > 0) {
      //Assuming That Data Will Always Be Returned
      const data = response.data.data;
      sortedFacts = data.sort((a: CatFactsInterface, b: CatFactsInterface) => (a.fact > b.fact) ? 1 : -1)
      setFacts(sortedFacts);
    }
    else {
      setErrorMessage("Error Retreiving Data");
    }
  }

  React.useEffect(() => {
    loadData();
  }, []);
  
  return (<ChakraProvider theme={theme}>
    <Box bg={useColorModeValue('gray.100', 'gray.800')} px={{ base: '6', md: '8' }} py="12">
      <Box as="section" maxW={{ base: 'xs', md: '3xl' }} mx="auto">

      { error.length > 0 &&
          <Text>{error}</Text>
      }
      { facts.length > 0 &&
        <SimpleGrid data-testid="fact-grid" columns={{ base: 1, md: 3 }} spacing="6">
          {facts.map((singleFact: CatFactsInterface) => {
            const { fact } = singleFact;
            return (
              <Card>
                {fact}
              </Card>
            )
          })}
        </SimpleGrid>
      }
      </Box>
    </Box>
  </ChakraProvider>);
}
